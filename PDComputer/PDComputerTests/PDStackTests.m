//
//  PDStackTests.m
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "PDStack.h"

@interface PDStackTests : XCTestCase
{
    PDStack *teststack;
}

@end

@implementation PDStackTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    teststack = [PDStack new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testStack {
    XCTAssertNotNil(teststack);
    XCTAssertEqual(teststack.count, 0);
    
    // -> [foo , 1]
    [teststack push:@"fooo"];
    [teststack push:[NSNumber numberWithInt:1]];
    
    XCTAssertEqual(teststack.count, 2);
    
    // test peek
    id peek = [teststack peek];
    
    XCTAssertEqual(teststack.count, 2);
    XCTAssertTrue([peek isKindOfClass:[NSNumber class]]);
    
    // test pop
    id pop = [teststack pop];
    
    XCTAssertEqual(teststack.count, 1);
    XCTAssertTrue([pop isKindOfClass:[NSNumber class]]);
    
    pop = [teststack pop];
    
    XCTAssertEqual(teststack.count, 0);
    XCTAssertTrue([pop isKindOfClass:[NSString class]]);
    
    pop = [teststack pop];
    XCTAssertNil(pop);
    
    peek = [teststack peek];
    XCTAssertNil(peek);
    
}


@end
