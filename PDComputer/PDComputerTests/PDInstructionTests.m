//
//  PDInstructionTests.m
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PDInstruction.h"

@interface PDInstructionTests : XCTestCase

@end

@implementation PDInstructionTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    NSUInteger testparameter = 1;
    PDInstruction *instruction = [[PDInstruction alloc] initWithInstruction:INSTRUCTION_PUSH parameter:testparameter];
    
    XCTAssertEqual(instruction.instruction,PDInstructionTypePush);
    XCTAssertEqual(instruction.parameter,testparameter);
    
    instruction = [[PDInstruction alloc] initWithInstruction:INSTRUCTION_PRINT parameter:testparameter];
    
    XCTAssertEqual(instruction.instruction,PDInstructionTypePrint);
    XCTAssertEqual(instruction.parameter,testparameter);
    
    instruction = [[PDInstruction alloc] initWithInstruction:INSTRUCTION_STOP parameter:testparameter];
    
    XCTAssertEqual(instruction.instruction,PDInstructionTypeStop);
    XCTAssertEqual(instruction.parameter,testparameter);
    
    instruction = [[PDInstruction alloc] initWithInstruction:INSTRUCTION_RET parameter:testparameter];
    
    XCTAssertEqual(instruction.instruction,PDInstructionTypeRet);
    XCTAssertEqual(instruction.parameter,testparameter);
    
    instruction = [[PDInstruction alloc] initWithInstruction:INSTRUCTION_CALL parameter:testparameter];
    
    XCTAssertEqual(instruction.instruction,PDInstructionTypeCall);
    XCTAssertEqual(instruction.parameter,testparameter);
    
    instruction = [[PDInstruction alloc] initWithInstruction:INSTRUCTION_MULT parameter:testparameter];
    
    XCTAssertEqual(instruction.instruction,PDInstructionTypeMult);
    XCTAssertEqual(instruction.parameter,testparameter);
    
    
}


@end
