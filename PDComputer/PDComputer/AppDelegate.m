//
//  AppDelegate.m
//  PDComputer
//
//  Created by Paulo de la Vina on 05/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "PDComputer.h"
#import "PDInstruction.h"

#define PRINT_TENTEN_BEGIN 50
#define MAIN_BEGIN 0

@interface AppDelegate ()
{
    PDComputer *computer;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if (computer == nil)
    {
        computer = [self setupComputer];
        ViewController *v = ((ViewController *) self.window.rootViewController);
        [v executeComputer:computer];
    }
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark Computer

- (PDComputer *) setupComputer
{
    // Create new computer with a stack of 100 addresses
    PDComputer *c = [[PDComputer alloc] initWithCapacity:100];
    
    // Instructions for the print_tenten function
    [[[[c setAddress:PRINT_TENTEN_BEGIN] insert:INSTRUCTION_MULT] insert:INSTRUCTION_PRINT] insert:INSTRUCTION_RET];
    
    // The start of the main function
    [[[c setAddress:MAIN_BEGIN] insert:INSTRUCTION_PUSH parameter:1009] insert:INSTRUCTION_PRINT];
    
    // Return address for when print_tenten function finishes
    [c insert:INSTRUCTION_PUSH parameter:6];
    
    // Setup arguments and call print_tenten
    [[[c insert:INSTRUCTION_PUSH parameter:101] insert:INSTRUCTION_PUSH parameter:10] insert:INSTRUCTION_CALL parameter:PRINT_TENTEN_BEGIN];
    
    // Stop the program
    [c insert:INSTRUCTION_STOP];
    
    [c setAddress:MAIN_BEGIN];
    return c;
}

@end
