//
//  ViewController.m
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import "ViewController.h"
#import "PDComputer.h"
#import "PDInstruction.h"

@interface ViewController ()
{
    
}

@property (nonatomic, weak) IBOutlet UITextView *terminal;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void) executeComputer: (PDComputer *) computer
{
    // add kvo for outsputstring to be displayed in the textview
    [computer addObserver:self forKeyPath:@"outputString" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
    [computer execute];
    
    // remove kvo after execution
    [computer removeObserver:self forKeyPath:@"outputString"];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if([keyPath isEqualToString:@"outputString"])
    {
        id newValue = [change objectForKey:NSKeyValueChangeNewKey];
        _terminal.text = [_terminal.text stringByAppendingString:[NSString stringWithFormat:@"\n# %@", newValue]];
    }
}


@end
