//
//  ViewController.h
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//


#import "PDComputer.h"
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (void) executeComputer: (PDComputer *) computer;

@end

