//
//  PDStack.h
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDStack : NSObject

- (void) push: (id) object;
- (id) pop;
- (id) peek;

- (NSUInteger) count;

@end
