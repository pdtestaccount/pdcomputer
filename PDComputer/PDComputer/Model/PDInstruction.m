//
//  PDInstruction.m
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import "PDInstruction.h"

NSString * const INSTRUCTION_MULT = @"MULT";
NSString * const INSTRUCTION_CALL = @"CALL";
NSString * const INSTRUCTION_RET = @"RET";
NSString * const INSTRUCTION_STOP = @"STOP";
NSString * const INSTRUCTION_PRINT = @"PRINT";
NSString * const INSTRUCTION_PUSH = @"PUSH";
NSString * const INSTRUCTION_ADD = @"ADD";

@implementation PDInstruction

- (instancetype) initWithInstruction: (NSString *) instruction parameter: (NSInteger) parameter
{
    self = [super init];
    
    if (instruction == INSTRUCTION_MULT)
    {
        self.instruction = PDInstructionTypeMult;
    }
    else if (instruction == INSTRUCTION_CALL)
    {
        self.instruction = PDInstructionTypeCall;
    }
    else if (instruction == INSTRUCTION_RET)
    {
        self.instruction = PDInstructionTypeRet;
    }
    else if (instruction == INSTRUCTION_STOP)
    {
        self.instruction = PDInstructionTypeStop;
    }
    else if (instruction == INSTRUCTION_PRINT)
    {
        self.instruction = PDInstructionTypePrint;
    }
    else if (instruction == INSTRUCTION_PUSH)
    {
        self.instruction = PDInstructionTypePush;
    }
    else if (instruction == INSTRUCTION_ADD)
    {
        self.instruction = PDInstructionTypeAdd;
    }
    
    self.parameter = parameter;
    
    return self;
}

- (BOOL)isProgramCounterChanger
{
    return  self.instruction == PDInstructionTypeRet || self.instruction == PDInstructionTypeCall;
}

@end
