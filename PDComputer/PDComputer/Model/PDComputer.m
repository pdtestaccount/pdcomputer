//
//  PDComputer.m
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import "PDComputer.h"
#import "PDStack.h"
#import "PDInstruction.h"

@interface PDComputer ()
{
    NSInteger instructionPointer;
    NSInteger programCounter;
}

@property (nonatomic, strong) PDStack *stack;
@property (nonatomic, strong) NSMutableArray *instructions;

@end

@implementation PDComputer


- (instancetype)initWithCapacity: (NSInteger) capacity
{
    self = [super init];
    
    instructionPointer = 0;
    
    _stack = [[PDStack alloc] init];
    _instructions = [[NSMutableArray alloc] initWithCapacity:capacity];
    
    // fill instructions with null
    for (int i =0; i < capacity; i++) {
        [_instructions addObject:[NSNull null]];
    }
    
    return self;
}

- (PDComputer *) setAddress: (NSUInteger) address
{
    if (address < _instructions.count)
    {
        instructionPointer = address;
    }
    return self;
}

- (PDComputer *) insert: (NSString *) instruction
{
    return [self insert:instruction parameter:0];
}

- (PDComputer *) insert: (NSString *) instruction parameter: (NSInteger) parameter
{
    PDInstruction *ins = [[PDInstruction alloc] initWithInstruction:instruction parameter:parameter];
    
    if (instructionPointer < _instructions.count)
    {
        [_instructions setObject:ins atIndexedSubscript:instructionPointer];
        instructionPointer++;
    }
    
    return self;
}

#pragma mark -
#pragma mark Program Execution Functions

- (void) execute
{
    programCounter = instructionPointer;
    
    BOOL terminate = NO;
    
    while (terminate == NO) {
        
        //handle programCounter out of bounds
        if (programCounter < 0 || programCounter >= _instructions.count)
        {
            self.outputString = @"Error: Address is out of bounds";
            return;
        }
        
        NSObject *data = [_instructions objectAtIndex:programCounter];
        
        if (data != nil && data != [NSNull null])
        {
            PDInstruction *instruction = (PDInstruction *) data;
            switch (instruction.instruction) {
                case PDInstructionTypeRet:
                    [self ret];
                    break;
                case PDInstructionTypeCall:
                    [self call:instruction.parameter];
                    break;
                case PDInstructionTypeMult:
                    [self mult];
                    break;
                case PDInstructionTypeAdd:
                    [self add];
                    break;
                case PDInstructionTypePush:
                    [self push: instruction.parameter];
                    break;
                case PDInstructionTypeStop:
                    terminate = YES;
                    break;
                case PDInstructionTypePrint:
                    self.outputString = [NSString stringWithFormat:@"%@" , [_stack pop]];
                    break;
                default:
                    break;
            }
            
            // if instruction did not change program counter, increment
            if (![instruction isProgramCounterChanger])
                programCounter++;
        }
        else
        {
            terminate = YES;
        }
        
    }
}

- (void) ret
{
    NSNumber *address = [_stack pop];
    programCounter = address.integerValue;
}

- (void) call: (NSInteger) address
{
    programCounter = address;
}

- (void) mult
{
    NSNumber *number1 = [_stack pop];
    NSNumber *number2 = [_stack pop];
    [_stack push:@( number1.integerValue * number2.integerValue )];
}

- (void) add
{
    NSNumber *number1 = [_stack pop];
    NSNumber *number2 = [_stack pop];
    [_stack push:@( number1.integerValue + number2.integerValue )];
}

- (void) push: (NSInteger) parameter
{
    [_stack push:@(parameter)];
}



@end
