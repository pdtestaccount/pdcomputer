//
//  PDStack.m
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import "PDStack.h"

@interface PDStack()

@property (nonatomic, strong) NSMutableArray *objects;

@end

@implementation PDStack

- (NSMutableArray *)objects
{
    if (_objects == nil)
    {
        _objects = [NSMutableArray array];
    }
    return _objects;
}

- (void)push:(id)object
{
    [self.objects addObject:object];
}

- (id)pop
{
    id object = [self.objects lastObject];
    if (object)
        [self.objects removeObject:object];
    return object;
}

- (id)peek
{
    return [self.objects lastObject];
}

- (NSUInteger)count
{
    return [self.objects count];
}


@end
