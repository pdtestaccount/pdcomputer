//
//  PDComputer.h
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDComputer : NSObject

@property (nonatomic, strong) NSString *outputString;

- (instancetype)initWithCapacity: (NSInteger) capacity;

- (PDComputer *) setAddress: (NSUInteger) address;

- (PDComputer *) insert: (NSString *) instruction;

- (PDComputer *) insert: (NSString *) instruction parameter: (NSInteger) parameter;

- (void) execute;

@end
