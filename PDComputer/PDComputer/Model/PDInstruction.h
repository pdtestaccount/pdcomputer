//
//  PDInstruction.h
//  PDComputer
//
//  Created by Paulo de la Vina on 03/06/2017.
//  Copyright © 2017 pdelavina. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const INSTRUCTION_MULT;
extern NSString * const INSTRUCTION_CALL;
extern NSString * const INSTRUCTION_RET;
extern NSString * const INSTRUCTION_STOP;
extern NSString * const INSTRUCTION_PRINT;
extern NSString * const INSTRUCTION_PUSH;
extern NSString * const INSTRUCTION_ADD;

typedef enum : NSUInteger {
    PDInstructionTypeNone,
    PDInstructionTypeMult,
    PDInstructionTypeCall,
    PDInstructionTypeRet,
    PDInstructionTypeStop,
    PDInstructionTypePrint,
    PDInstructionTypePush,
    PDInstructionTypeAdd
} PDInstructionType;

@interface PDInstruction : NSObject

@property PDInstructionType instruction;
@property NSInteger parameter;

- (instancetype) initWithInstruction: (NSString *) instruction parameter: (NSInteger) parameter;

// returns YES if the program counter is changed by this instruction
- (BOOL) isProgramCounterChanger;
@end
